package sample.controller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

import sample.dataaccess.ConsultasImplementation;
import sample.model.Cliente;


public class Controller implements Initializable {

    //Elementos de la tabla
    @FXML
    private TableView<Cliente> table;
    @FXML
    private TableColumn<Cliente, String> papellido;
    @FXML
    private TableColumn<Cliente, String> sapellido;
    @FXML
    private TableColumn<Cliente, String> pnombre;
    @FXML
    private TableColumn<Cliente, String> onombre;
    @FXML
    private TableColumn<Cliente, String> pais;
    @FXML
    private TableColumn<Cliente, String> tipoid;
    @FXML
    private TableColumn<Cliente, String> numid;
    @FXML
    private TableColumn<Cliente, String> correo;
    @FXML
    private TableColumn<Cliente, String> fechaing;
    @FXML
    private TableColumn<Cliente, String> area;
    @FXML
    private TableColumn<Cliente, String> estado;
    @FXML
    private TableColumn<Cliente, String> fechareg;
    @FXML
    private Pagination pagination;

    //Elementos del filtro
    @FXML
    private TextField filpnombre;
    @FXML
    private TextField filonombres;
    @FXML
    private TextField filpapellido;
    @FXML
    private TextField filsapellido;
    @FXML
    private TextField filnumident;
    @FXML
    private TextField filcorreo;
    @FXML
    private ComboBox<String> filtident;
    @FXML
    private ComboBox<String> filpais;
    @FXML
    private ComboBox<String> filestado;


    ObservableList<Cliente> listview = FXCollections.observableArrayList();
    ConsultasImplementation consultas;
    ObservableList<String> tiposIdentificaciones = FXCollections.observableArrayList("", "Cedula de Ciudadania", "Cedula de Extranjeria",
            "Targeta de Identidad", "Registro Civil");

    ObservableList<String> paises = FXCollections.observableArrayList("", "Colombia", "Estados Unidos");
    ObservableList<String> estados = FXCollections.observableArrayList("", "Activo", "Inactivo");
    int itemPorPg = 10;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        filtident.setItems(tiposIdentificaciones);
        filpais.setItems(paises);
        filestado.setItems(estados);
        consultas = new ConsultasImplementation();

        papellido.setCellValueFactory(new PropertyValueFactory<>("primer_apellido"));
        sapellido.setCellValueFactory(new PropertyValueFactory<>("segundo_apellido"));
        pnombre.setCellValueFactory(new PropertyValueFactory<>("primer_nombre"));
        onombre.setCellValueFactory(new PropertyValueFactory<>("otros_nombres"));
        pais.setCellValueFactory(new PropertyValueFactory<>("pais_empleo"));
        tipoid.setCellValueFactory(new PropertyValueFactory<>("tipo_identificacion"));
        numid.setCellValueFactory(new PropertyValueFactory<>("numero_identificacion"));
        correo.setCellValueFactory(new PropertyValueFactory<>("correo_electronico"));
        fechaing.setCellValueFactory(new PropertyValueFactory<>("fecha_ingreso"));
        area.setCellValueFactory(new PropertyValueFactory<>("area"));
        estado.setCellValueFactory(new PropertyValueFactory<>("estado"));
        fechareg.setCellValueFactory(new PropertyValueFactory<>("fecha_registro"));

        mostrarUsuarios(new ActionEvent());
    }

    @FXML
    private void agregarCliente(ActionEvent event) {
        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("../view/clienteDialog.fxml"));
            Parent root = loader.load();
            ClienteDialogController controlador = loader.getController();
            Scene scene = new Scene(root, 500, 500);
            Stage stage = new Stage();
            stage.setTitle("Agregar Cliente");
            stage.getIcons().add(new Image(getClass().getResourceAsStream("../images/icono_cidenet.png")));
            stage.initModality(Modality.APPLICATION_MODAL);
            stage.setScene(scene);
            stage.showAndWait();
            mostrarUsuarios(new ActionEvent());
        } catch (IOException e) {
            Logger.getLogger(Controller.class.getName()).log(Level.SEVERE, null, e);
        }
    }

    private Node createPage(int pageIndex) {
        int fromIndex = pageIndex * itemPorPg;
        int toIndex = Math.min(fromIndex + itemPorPg, listview.size());
        table.setItems(FXCollections.observableArrayList(listview.subList(fromIndex, toIndex)));
        return table;
    }

    @FXML
    public void mostrarUsuarios(ActionEvent event) {
        try {
            listview = FXCollections.observableArrayList();
            listview = consultas.consultaUsuarios(filpnombre.getText().trim(), filonombres.getText().trim(), filpapellido.getText().trim(),
                    filsapellido.getText().trim(), filtident.getSelectionModel().getSelectedItem(), filnumident.getText().trim(),
                    filpais.getSelectionModel().getSelectedItem(), filcorreo.getText().trim(), filestado.getSelectionModel().getSelectedItem());
            int pageCount = ((listview.size()-1) / itemPorPg)+1;
            pagination.setPageCount(pageCount);
            pagination.setPageFactory(this::createPage);
            //table.setItems(listview);

        } catch (Exception e) {
            Logger.getLogger(Controller.class.getName()).log(Level.SEVERE, null, e);
        }
    }

    @FXML
    public void borrarUsuario(ActionEvent event) {
        Cliente cliente = this.table.getSelectionModel().getSelectedItem();
        if (cliente == null) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setHeaderText(null);
            alert.setTitle("ERROR");
            alert.setContentText("Debe Seleccinar un Registro");
            alert.showAndWait();
        } else {
            Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
            alert.setHeaderText(null);
            alert.setTitle("Confirmación");
            alert.setContentText("¿Estas seguro de eliminar el usuario?");
            Optional<ButtonType> action = alert.showAndWait();

            if (action.get() == ButtonType.OK) {
                consultas.eliminarCliente(cliente.getId_cliente());
                mostrarUsuarios(new ActionEvent());
                Alert alert2 = new Alert(Alert.AlertType.INFORMATION);
                alert2.setHeaderText(null);
                alert2.setTitle("Información");
                alert2.setContentText("Se ha eliminado correctamente el usuario");
                alert2.showAndWait();
            }
        }
    }

    @FXML
    public void editarUsuario(ActionEvent event) {
        Cliente cliente = this.table.getSelectionModel().getSelectedItem();
        if (cliente == null) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setHeaderText(null);
            alert.setTitle("ERROR");
            alert.setContentText("Debe Seleccinar un Registro");
            alert.showAndWait();
        } else {
            try {
                FXMLLoader loader = new FXMLLoader(getClass().getResource("../view/clienteDialog.fxml"));
                Parent root = loader.load();
                ClienteDialogController controlador = loader.getController();
                controlador.inicializarAtributos(cliente, false);
                Scene scene = new Scene(root, 500, 500);
                Stage stage = new Stage();
                stage.setTitle("Editar Cliente");
                stage.getIcons().add(new Image(getClass().getResourceAsStream("../images/icono_cidenet.png")));
                stage.initModality(Modality.APPLICATION_MODAL);
                stage.setScene(scene);
                stage.showAndWait();
                mostrarUsuarios(new ActionEvent());
            } catch (IOException e) {
                Logger.getLogger(Controller.class.getName()).log(Level.SEVERE, null, e);
            }

        }
    }


}
